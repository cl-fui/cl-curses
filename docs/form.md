# Form subsystem

Functions are generally prefixed by form- and follow their C counterparts, with some exceptions to make the interface more consistent.  The low-level bindings& are never renamed.

## Naming exceptions

| C name | Lisp name |
|---|---|
| new_field | field-new |
| new_form | form-new |
| free-field | field-free |
| free-form | form-free |
| field_count  | form-field-count  |
| move_field | field-move |
| new_page | field-new-page |
| pos_form_cursor | form-pos-cursor |
| post_form | form-post |
| unpost_form | form-unpost |

## Settables

 setf works on the following bindings: 
 
| | | |
|---|---|---|

## Fields

To build a conforming null-terminated field-pointer-array for form creation, FUI provides the following functions:

* `FIELDS-NEW` and `FIELDS-FREE` to create field-lists.  `FIELDS-NEW` parameter is a list of field-initializers; each field is described with a sublist of 4 to 6 elements.


## FORMS

Forms are created with fields (addressed above), and normally `FORM-NEW` and `FORM-FREE` will do what is needed.  

If you are holding and reusing a `FIELDS` foreign object for multiple invocations, make sure that `FORM-NEW` and `FORM-FREE` do not deallocate your fields.  Normally, `FORM-NEW` will delete fields on error, so pass it `:FREE-FIELDS-ON-ERROR NIL`.  `FORM-FREE` will always deallocate fields unless you pass it NIL as the fist argument.

## Driver

Several 'error codes' returned by driver are not errors but normal conditions that require further processing - such as E-UNKNOWN-COMMANDS.  These are returned as numeric codes:

| for | reply | description  |
|---|---|---|
| 0  | NIL | processed  |
| -8  | E-UNKNOWN-COMMAND | not a driver key |
| -9  | E-NO-MATCH | match failed |
| -12 | E-REQUEST-DENIED | out of bounds, etc |

In simple cases the reply may be ignored.  Otherwise, E-UNKNOWN-COMMAND may require user processing; E-NO-MATCH and E-REQUEST-DENIED may ring a bell, etc.

## Callbacks

There are 4 callback hooks for field-init, field-term, form-init and form-term.  See [ncurses hook docs](http://invisible-island.net/ncurses/man/form_hook.3x.html) for calling context.  Callbacks are invoked with a single form pointer parameter

To define a callback function, use:
`(defcb cbname (formname)...)`; formname is an arbitrary name for the form parameter.

To install a callback function, use:
`(setf (on-..) (callback cbname))`
