# cl-curses

CL-CURSES is a set of cffi bindings to the NCURSES library for building partable text user interfaces.

Other curses implementations appear to be incomplete, outdated, or diverge by introducing higher-level constructs.  CL-CURSES tries to be a complete and comprehensive ncurses library.  

All low-level ncurses functions are provided with a & suffix and usual _ to - conversion.  `SETF` works where it's sensible, and a few names are modified to make the namespace more sensible (i.e. new_menu -> menu-new).  These are documented in appropriate doc files.

Panels, menus and forms are implemented.

## Status: pre-release

22-Oct-2018 - Form subsystem implemented
20-Oct-2018 - Menu subsystem implemented
15-Oct-2018 - Panel subsystem implemented  
06-Nov-2017 - core implementation complete - testing.

## EMACS/Slime users:

NCURSES runs in a real terminal.  These bindings control the terminal that started Lisp.  If you start your CL implementation from a terminal, things make sense. 

If you use Slime and Emacs, take a minute to figure out what's happening.

First of all, you cannot just start SLIME - there is no terminal ncurses can use (and an emacs buffer is not a terminal).  So you need to start a separate swank session in a real terminal, and connect to it with slime-connect.

Now SWANK is probably running in a thread talking to SLIME, but the terminal most likely has a separate REPL in it.  The REPL will eat characters and print things, messing up your output.  So you probably want to sleep the thread.

Recipes:

SBCL:`sbcl --eval "(progn (ql:quickload '(:swank) :silent t)" --eval "(swank:create-server :port 4006 :dont-close t)"`
Roswell: `ros run -e "(progn (ql:quickload '(:swank) :silent t)" -e "(swank:create-server :port 4006 :dont-close t) (loop (sleep 10000))) "`

Connect to it from emacs with `slime-connect`, entering the same port (4006 in this case).

Test with something like:
```
(initscr)
(printw "hello")
(refresh)
(getch)
(endwin)
```
In most cases you don't want CL REPL to continue in the terminal - it will steal keystrokes and try to evaluate them!  So append something like `(loop (sleep 10000))` to your invocation...

In some cases swank may work better with `:style :fd-handler`.


## Notes

- booleans are generally Lispified
- printw functions use a c-style format string, but require cffi types for parameters, e.g. `(printf "Hello number %d" :int 3)`  This is a CFFI feature.
- \n and other escapes do not seem to work (priority med)

## Not Implemented 

While the goal is a reasonably complete implementation, a decision was made to not bother with:

- Input functions without a count - such as getstr.  Use getnstr, etc.
- Scan functions - not particularly useful...(priority low)

## Related projects:
- cl-ncurses  (obsolete uffi, no cffi)
- cl-charms   (incomplete, unhappy with some choices made)
- croatan     (diverging from ncurses)

