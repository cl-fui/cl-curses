(in-package :nc)
(defparameter *form* nil)

(cffi:defcvar ("TYPE_ALPHA" *TYPE-ALPHA* :library libform :read-only t) :pointer)
(cffi:defcvar ("TYPE_ALNUM" *TYPE-ALNUM* :library libform :read-only t) :pointer)

(cffi:defcvar ("TYPE_ENUM" *TYPE-ENUM* :library libform :read-only t) :pointer)
(cffi:defcvar ("TYPE_INTEGER" *TYPE-INTEGER* :library libform :read-only t) :pointer)
(cffi:defcvar ("TYPE_NUMERIC" *TYPE-NUMERIC* :library libform :read-only t) :pointer)
(cffi:defcvar ("TYPE_REGEXP" *TYPE-REGEXP* :library libform :read-only t) :pointer)

(cffi:defcvar ("TYPE_IPV4" *TYPE-IPV4* :library libform :read-only t) :pointer)

;;(defcfun ("new_fieldtype" ftype-new :pointer)   ())

;; field routines
(defcfun ("new_field" new-field&) :pointer
  (height :int) (width :int) (toprow :int) (leftcol :int) (offscreen :int) (nbuffers :int))
(defcfun ("dup_field" dup-field&) :pointer
  (field :pointer) (toprow :int) (leftcol :int))
(defcfun ("link_field" link-field&) :pointer 
  (field :pointer) (toprow :int) (leftcol :int))
(defcfun ("free_field" free-field&) :int
  (field :pointer) )

(defcfun ("field-info" field-info&) :int
  (field :pointer)
  (height (:pointer :int)) (width (:pointer :int))
  (toprow (:pointer :int)) (leftcol (:pointer :int))
  (offscreen (:pointer :int)) (nbuffers (:pointer :int)))

(defcfun ("dynamic-field-info" dynamic-field-info&) :int
  (field :pointer) (rows (:pointer :int)) (cols (:pointer :int)) (max (:pointer :int)) )

(defcfun ("field_fore" field-fore&) :long (field :pointer))
(defcfun ("field_back" field-back&) :long (field :pointer))
(defcfun ("field_pad"  field-pad&) :int   (field :pointer))
(defcfun ("set_field_fore" set-field-fore&) :int (field :pointer) (fore :long))
(defcfun ("set_field_back" set-field-back&) :int (field :pointer) (back :long))
(defcfun ("set_field_pad"  set-field-pad&) :int   (field :pointer) (pad :int))

;; http://invisible-island.net/ncurses/man/form_field_buffer.3x.html
(defcfun ("field_buffer" field-buffer&) :pointer
  (field :pointer) (buf :int))
(defcfun ("set_field_buffer" set-field-buffer&) :int
  (field :pointer) (buf :int) (value :pointer))
(defcfun ("field_status" field-status&) :int
  (field :pointer))
(defcfun ("set_field_status" set-field-status&) :int
  (field :pointer) (status :int))
(defcfun ("set_max_field" set-max-field&) :int
  (field :pointer) (max :int))
;; http://invisible-island.net/ncurses/man/form_field.3x.html
(defcfun ("set_form_fields" set-form-fields&) :int  (field :pointer) (fields :pointer))
(defcfun ("form_fields" form-fields&) :pointer   (field :pointer))
(defcfun ("field_count" field-count&) :int       (form :pointer))
(defcfun ("move_field"  move-field&)  :int       (form :pointer) (frow :int) (fcol :int))
;;http://invisible-island.net/ncurses/man/form_new_page.3x.html
(defcfun ("new_page" new-page&) :int (field :pointer))
(defcfun ("set_new_page" set-new-page&) :int (field :pointer) (page :bool))
;;http://invisible-island.net/ncurses/man/form_field_just.3x.html
(defcfun ("field_just" field-just&) :int (field :pointer))
(defcfun ("set_field_just" set-field-just&) :int (field :pointer) (just :int))
;;http://invisible-island.net/ncurses/man/form_field_userptr.3x.html
(defcfun ("field_userptr" field-userptr&) :pointer (field :pointer))
(defcfun ("set_field_userptr" set-field-userptr&) :int (field :pointer) (userptr :pointer))
;;http://invisible-island.net/ncurses/man/form_field_opts.3x.html
(defcfun ("field_opts" field-opts&) :int (field :pointer))
(defcfun ("set_field_opts" set-field-opts&) :int (field :pointer) (field-opts :int))
(defcfun ("field_opts_on" field-opts-on&) :int (field :pointer) (field-opts :int))
(defcfun ("field_opts_off" field-opts-off&) :int (field :pointer) (field-opts :int))
;; http://invisible-island.net/ncurses/man/form_field_validation.3x.html
;; TODO

;; FORM
(defcfun ("new_form" new-form&) :pointer (fields :pointer))
(defcfun ("free_form" free-form&) :int  (form :pointer) )
;;http://invisible-island.net/ncurses/man/form_page.3x.html
(defcfun ("current_field" current-field&) :pointer (form :pointer))
(defcfun ("set_current_field" set-current-field&) :int (form :pointer) (field :pointer))
(defcfun ("unfocus_current_field" unfocus-current-field&) :int (form :pointer))
(defcfun ("form_page" form-page&) :int (form :pointer))
(defcfun ("set_form_page" set-form-page&) :int (form :pointer) (page :int))
(defcfun ("field_index" field-index&) :int (field :pointer))
;;http://invisible-island.net/ncurses/man/form_win.3x.html
(defcfun ("form_win" form-win&) :pointer (form :pointer))
(defcfun ("set_form_win" set-form-win&) :int (form :pointer) (win :pointer))
(defcfun ("form_sub" form-sub&) :pointer (form :pointer))
(defcfun ("set_form_sub" set-form-sub&) :int (form :pointer) (sub :pointer))
(defcfun ("scale_form" scale-form&)
    :int (form :pointer)(rows (:pointer :int))(columns (:pointer :int)))
;;------------------------------------------------------------------
;; hooks
(defcfun ("field_init" field-init&) :pointer
       (form :pointer))
(defcfun ("set_field_init" set-field-init&) :int
       (form :pointer) (callback :pointer))
(defcfun ("field_term" field-term&) :pointer
       (form :pointer))
(defcfun ("set_field_term" set-field-term&) :int
       (form :pointer)(callback :pointer))
(defcfun ("form_init" form-init&) :pointer
       (form :pointer))
(defcfun ("set_form_init" set-form-init&) :int
       (form :pointer)(callback :pointer))
(defcfun ("form_term" form-term&) :pointer
       (form :pointer))
(defcfun ("set_form_term" set-form-term&) :int
       (form :pointer)(callback :pointer))
;;------------------------------------------------------------------
;; http://invisible-island.net/ncurses/man/form_post.3x.html
(defcfun ("post_form" post-form&) :int (form :pointer))
(defcfun ("unpost_form" unpost-form&) :int (form :pointer))
;;http://invisible-island.net/ncurses/man/form_cursor.3x.html
(defcfun ("pos_form_cursor" pos-form-cursor&) :int (form :pointer))
;;http://invisible-island.net/ncurses/man/form_driver.3x.html
(defcfun ("form_driver" form-driver&) :int (form :pointer) (char :int))
;;http://invisible-island.net/ncurses/man/form_userptr.3x.html
(defcfun ("form_userptr" form-userptr&) :pointer (form :pointer))
(defcfun ("set_form_userptr" set-form-userptr&) :int (form :pointer) (userptr :pointer))
;;http://invisible-island.net/ncurses/man/form_opts.3x.html
(defcfun ("form_opts" form-opts&) :int (field :pointer))
(defcfun ("set_form_opts" set-form-opts&) :int (field :pointer) (form-opts :int))
(defcfun ("form_opts_on" form-opts-on&) :int (field :pointer) (form-opts :int))
(defcfun ("field_opts_off" form-opts-off&) :int (field :pointer) (form-opts :int))
;; TODO: form-request-by-name

;;http://invisible-island.net/ncurses/man/form_data.3x.html
(defcfun ("data_ahead" data-ahead&) :int (form :pointer))
(defcfun ("data_behind" data-behind&) :int (form :pointer))

;;==============================================================================
;;==============================================================================
;; Create an accessor-style binding wrapper and a matching setter.  Both allow
;; the form parameter to be optional, and in the last place.  Get-type and
;; set-type are used by the generated check macro.  set-type is pretty much
;; always int (status returned by the setter binding); get-type is whatever it
;; is, but has anomoulous situations: to wit, a boolean is returned as an int
;; by the bindings, but cannot be cffi:bool because it may also be an errorcode.
;;
(defmacro form-settable (lispname &key setter& getter& (get-type :int) (set-type :status))
  (unless getter& ;; symbol of the accessor binding
    (setf getter& (find-symbol (concatenate 'string (symbol-name lispname) "&"))))
  (unless setter& ;; symbol of the setter binding
    (setf setter& (find-symbol (concatenate 'string "SET-" (symbol-name getter&) ))))
  `(progn
     (defun ,lispname (&optional (form *form*))
       (check (,getter& ,get-type) form))
     (defsetf ,lispname (&optional (form *form*)) (val)
       (let ((set-type ',set-type)
	     (setter& ',setter&))
	 `(check (,setter& ,set-type) ,form ,val )))
     (export ',lispname)))




(xdefun field-info (field)
	"Return field values: h w y x offscreen buffers"
  (with-foreign-objects ((height :int) (width :int)
			 (toprow :int) (leftcol :int)
			 (offscreen :int) (nbuffers :int))
    (check (field-info& :status) field height width toprow leftcol offscreen nbuffers)
    (values (mem-ref height :int) (mem-ref width :int)
	    (mem-ref toprow :int) (mem-ref leftcol :int)
	    (mem-ref offscreen :int) (mem-ref nbuffers :int))))

(xdefun dynamic-field-info (field)
	"Return dynamic field values: h w max"
  (with-foreign-objects ((rows :int) (cols :int)(max :int) )
    (check (dynamic-field-info& :status) field rows cols max)
    (values (mem-ref rows :int) (mem-ref cols :int) (mem-ref max :int))))
;;http://invisible-island.net/ncurses/man/form_field_attributes.3x.html
(xdefun field-fore (field) (check (field-fore& :long) field))
(xdefun field-back (field) (check (field-back& :long) field))
(xdefun field-pad  (field) (check (field-pad& :int) field))
(xdefun set-field-fore (field fore) (check (set-field-fore& :status) field fore))
(xdefun set-field-back (field back) (check (set-field-back& :status) field back))
(xdefun set-field-pad  (field pad) (check (set-field-pad& :status) field pad))
(xdefsetf field-fore set-field-fore)
(xdefsetf field-back set-field-fore)
(xdefsetf field-fore set-field-fore)
;;  http://invisible-island.net/ncurses/man/form_field_buffer.3x.html
(xdefun field-buffer (field buf) (check (field-buffer& :fp) field buf))
(xdefun set-field-buffer (field buf val) (check (set-field-buffer& :status) field buf val))
(xdefsetf field-buffer set-field-buffer)
(xdefun field-status (field) (check (field-status& :bool) field ))
(xdefun set-field-status (field status) (check (set-field-status& :status) field status))
(xdefsetf field-status set-field-status)
(xdefun set-max-field (field max) (check (set-max-field& :status) field max))
(xdefsetf max-field set-max-field)
;; http://invisible-island.net/ncurses/man/form_field.3x.html
(xdefun form-fields (&optional (form *form*))
	(check (form-fields& :fp) form))
(defun set-form-fields (form fields) (check (set-form-fields& :status) form fields))
(xdefsetf form-fields set-form-fields)
()
(xdefun form-field-count (&optional (form *form*))
	(check (field-count& :int) form))
(xdefun field-move (field frow fcol) (check (move-field& :status) field frow fcol)  )
;;http://invisible-island.net/ncurses/man/form_new_page.3x.html
(xdefun field-new-page (field) (check (new-page& :bool) field))
(xdefun set-field-new-page (field flag) (check (set-new-page& :status) field flag))
(xdefsetf field-new-page set-field-new-page)
;;http://invisible-island.net/ncurses/man/form_field_just.3x.html
(xdefun field-just (field) (check (field-just& :int) field))
(xdefun set-field-just (field just) (check (set-field-just& :status) field just))
(xdefsetf field-just set-field-just)
;;http://invisible-island.net/ncurses/man/form_field_userptr.3x.html
(xdefun field-userptr (field) (check (field-userptr& :fp) field))
(xdefun set-field-userptr (field userptr)
	(check (set-field-userptr& :status) field userptr))
(xdefsetf field-userptr set-field-userptr)
;;http://invisible-island.net/ncurses/man/form_field_opts.3x.html
(xdefun field-opts (field) (check (field-opts& :int) field))
(xdefun set-field-opts (field opts)
	(check (set-field-opts& :status) field opts))
(xdefsetf field-opts set-field-opts)
(xdefun field-opts-on (field opts) (check (field-opts-on& :status) field opts))
(xdefun field-opts-off (field opts) (check (field-opts-off& :status) field opts))
;;http://invisible-island.net/ncurses/man/form_page.3x.html
(form-settable form-current-field :getter& current-field& :get-type :fp)
(xdefun form-unfocus-current-field (&optional (form *form*))
	(check (unfocus-current-field& :status) form))
(form-settable form-page)
(xdefun field-index (field)
	(check (field-index& :int) field))
;;http://invisible-island.net/ncurses/man/form_win.3x.html
(form-settable form-win :get-type :fp)
(form-settable form-sub :get-type :fp)

;; TODO: error check
(xdefun form-scale (&optional (form *form*))
  (with-foreign-objects ((rows :int)(cols :int))
    (scale-form& form rows cols)
    (values (mem-ref rows :int)(mem-ref cols :int))))
;;http://invisible-island.net/ncurses/man/form_post.3x.html
(xdefun form-post (&optional (form *form*))
	(check (post-form& :status) form))
(xdefun form-unpost (&optional (form *form*))
	(check (unpost-form& :status) form))
;;http://invisible-island.net/ncurses/man/form_cursor.3x.html
(xdefun form-pos-cursor(&optional (form *form*))
	(check (pos-form-cursor& :status) form))
;;http://invisible-island.net/ncurses/man/form_driver.3x.html
(xdefun form-driver(ch &optional (form *form*))
	(check (form-driver& :status) form ch))
;;http://invisible-island.net/ncurses/man/form_userptr.3x.html
;; null-pointer is ok
(xdefun form-userptr (form) (check (form-userptr& :fp) form))
(xdefun set-form-userptr (form userptr)
	(check (set-form-userptr& :status) form userptr))
(xdefsetf form-userptr set-form-userptr)
;;http://invisible-island.net/ncurses/man/form_opts.3x.html
(form-settable form-opts)
(xdefun form-opts-on (form opts) (check (form-opts-on& :status) form opts))
(xdefun form-opts-off (form opts) (check (form-opts-off& :status) form opts))
;;http://invisible-island.net/ncurses/man/form_data.3x.html
(xdefun form-data-ahead (form) (check (data-ahead& :bool) form))
(xdefun form-data-behind (form) (check (data-behind& :bool) form))
;;====================================================================
;;==============================================================================
;; http://invisible-island.net/ncurses/man/form_field_new.3x.html
(xdefun field-new (h w y x &optional (offscreen 0) (nbuffers 0))
  (check (new-field& :fp) h w y x offscreen nbuffers))
(xdefun field-dup (field y x)
  (check (dup-field& :fp) field y x))
(xdefun field-link (field y x)
  (check (link-field& :fp) field y x))
(xdefun field-free (field)
	(check (free-field& :status) field))
;;==============================================================================
;; Fields - a null-terminated foreign array of field pointers...
;; each field is described by a sublist with (h w y x offscreen nbuffers)
;; Todo: deallocate partially-allocated fields on error midway
(xdefun fields-new (field-list)
  (let* ((cnt (length field-list))
	 (fields (foreign-alloc :pointer :initial-element (null-pointer)
				:count (1+ cnt))))
    (setf (mem-aref fields :pointer cnt)(null-pointer));; NUL-TERM fields!
    (loop
       for i from 0
       for field-data in field-list do
	 (setf (mem-aref fields :pointer i)
	       (apply #'field-new field-data)))
    (values fields cnt)))

(xdefun fields-free (fields)
  (loop for i from 0
     for field = (mem-aref fields :pointer i)
     until (null-pointer-p field)
     do (field-free field))
  0)
;;==============================================================================
;; form new
(xdefun form-new (fields &key (free-fields-on-error t))
  (let ((result (new-form& fields)))
    (if (null-pointer-p result)
	(progn (when free-fields-on-error (fields-free fields))
	       (err-form result))
	(setf *form* result))))
(xdefun form-free (&optional (free-fields t) (form *form*))
  (let ((fields (form-fields form))
	(result (free-form& form)))
    (when free-fields (fields-free fields))
    (if (zerop result)
	result
	(err-form result))))
;;=============================================================================
;; callbacks
(xdefun field-on-init (&optional (form *form*))(field-init& form) )
(xdefun field-on-term (&optional (form *form*))(field-term& form) )
(xdefun form-on-init (&optional (form *form*))(form-init& form) )
(xdefun form-on-term (&optional (form *form*))(form-term& form) )

(defun set-form-on-term (cb &optional (form *form*))
  (set-form-term& form cb) )
(defun set-form-on-init (cb &optional (form *form*))
  (set-form-init& form cb) )
(defun set-field-on-term (cb &optional (form *form*))
  (set-field-term& form cb) )
(defun set-field-on-init (cb &optional (form *form*))
  (set-field-init& form cb) )
(xdefsetf form-on-init set-form-on-init)
(xdefsetf form-on-term set-form-on-term)
(xdefsetf field-on-init set-field-on-init)
(xdefsetf field-on-term set-field-on-term)
