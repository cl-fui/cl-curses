# Menu subsystem

Functions are generally prefixed by menu- and follow their C counterparts, with some exceptions to make the interface more consistent.  The low-level bindings& are never renamed.

## Naming exceptions

| C name | Lisp name |
|---|---|
| current_item  | menu-current-item  |
| top_row | menu-top-row  |
| pos-menu-cursor | menu-pos-cursor |
| post-menu | menu-post |
| unpost-menu | menu-unpost |
| item-init | item-on-init |
| item-term | item-on-term |
| menu-init | menu-on-init |
| menu-term | menu-on-term |

## Settables



 setf works on the following bindings: 
 
| | | |
|---|---|---|
| item-opts  | menu-grey  | menu-top-row  |
| item-userptr  | menu-items  | menu-win  |
| item-value  | menu-mark  | on-item-init  |
| menu-back  | menu-opts  | on-item-term  |
| menu-current-item  | menu-pad  | on-menu-init  |
| menu-fore  | menu-pattern  | on-menu-init  |
| menu-format  | menu-sub  |   |
| pos-menu-cursor |

## Items

To build a conforming null-terminated item-pointer-array for menu creation, FUI provides the following functions:

* `ITEMS-NEW` and `ITEMS-FREE` to create item-lists; each may be a string, an alist (name . desc), or a previously created raw item.

For example:
```
(items-new '("hello" ("world" . "desc")))
```
Note: ITEM-FREE calls foreign-string-free on its strings; when using custom-built or reused items, pay attention to this fact.

## Menus

Menus are created with items (addressed above), and normally `MENU-NEW` and `MENU-FREE` will do what is needed.  

If you are holding and reusing the ITEMS foreign object for multiple invocations, make sure that `MENU-NEW` and `MENU-FREE` do not deallocate your items.  Normally, `MENU-NEW` will delete items on error, so pass it `:FREE-ITEMS-ON-ERROR NIL`.  `MENU-FREE` will always deallocate items unless you pass it NIL as the fist argument.

## Driver

Keystrokes are usually mapped to driver requests (down arrow key to REQ-DOWN-ITEM, etc).  `menu-xtable` provides the generic mappings as an alist, and `menu-xlate` does the mapping, passing unknown keys without change.

The requests should be sent to `menu-driver`.  Several possible return values of the binding are confusingly not errors.  DRIVER therefore raises ERR-MENU condition only on real errors, and returns the following symbols otherwise:

| for | reply | description  |
|---|---|---|
| 0  | NIL | processed  |
| -8  | E-UNKNOWN-COMMAND | not a driver key |
| -9  | E-NO-MATCH | match failed |
| -12 | E-REQUEST-DENIED | out of bounds, etc |

In simple cases the reply may be ignored.  Otherwise, E-UNKNOWN-COMMAND may require user processing; E-NO-MATCH and E-REQUEST-DENIED may ring a bell, etc.

## Callbacks

There are 4 callback hooks for item-init, item-term, init and term.  See [ncurses hook docs](http://invisible-island.net/ncurses/man/menu_hook.3x.html) for calling context.  Callbacks are invoked with a single menu pointer parameter

To define a callback function, use:
`(menu-defcb cbname (menuname)...)`; menuname is an arbitrary name for the menu parameter.

To install a callback function, use:
`(setf (on-..) (callback cbname))`

## User Pointers

Curses has a provision for storing a 'user pointer' in each item, and in the menu itself.  In practice, it makes little sense for Lisp users as we generally want to store a Lisp object (which cannot be done with a foreign pointer).  

userptr functions store :LONG integers - even though function names imply ptrs.  These may be resolved to lisp objects with an array.

## Notes:
SET-MENU-MARK takes a string and plugs it into it innards - cffi:string cannot be used for the argument.
