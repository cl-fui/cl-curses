(in-package :nc)
(defctype chtype :unsigned-long)

(defmacro xdefconst (name value)
  `(progn
     (defparameter ,name ,value)
     (export ',name)))

(defmacro xdefsetf (name setter)
  `(progn
     (defsetf ,name ,setter)
     (export ',name)))
(defmacro xdefmacro (name &body body)
  `(progn
     (defmacro ,name ,body)
     (export ',name)))
;; check if error

(defmacro check ((lowcall ret-type) &body body)
  (let ((lsym (find-symbol (symbol-name lowcall) )))
    (case ret-type
      (:fp
       `(let ((val (,lsym ,@body)))
	  (declare (optimize (speed 3) (safety 0)))
	  (declare (type foreign-pointer val))
	  (if (null-pointer-p val)
	      (progn (err-menu -1) ;;(values val)
		     )
	      val)))
      (:string
       `(let ((val (,lsym ,@body)))
	  (declare (optimize (speed 3) (safety 0)))
	  (declare (type foreign-pointer val))
	  (if (null-pointer-p val)
	      (progn (err-menu -1))
	      (foreign-string-to-lisp val))))
      ((:int :long )
       `(let ((val (,lsym ,@body)))
	  (declare (optimize (speed 3) (safety 0)))
	  (declare (type fixnum val))
	  (if (minusp val)
	      (progn (err-menu val) (values val)
		     )
	      (values val))))
      (:bool
       `(let ((val (,lsym ,@body)))
	  (declare (optimize (speed 3) (safety 0)))
	  (declare (type fixnum val))
	  (if (minusp val)
	      (progn (err-menu val) (values val) )
	      (plusp val))))
      (:status
       `(let ((val (,lsym ,@body)))
	  (declare (optimize (speed 3) (safety 0)))
	  (declare (type fixnum val))
	  (if (minusp val)
	      (progn (err-menu val) (values val))
	      (values))))
      (t (error "menucheck: invalid return type")))))
;; An exported defun
(defmacro xdefun (name &body body)
  `(progn
     (defun ,name ,@body)
     (export ',name)))

(defmacro xdefcfun ((cname lname &optional (hint :check) (exname lname)) &body body)
  (let* ((rettype (if (consp (car body))
		      (caar body)
		      (car body)))
	 (parampos (if (stringp (cadr body))
		       (cddr body)
		       (cdr body)))
	 (rest nil)
	 (params (if (consp parampos)
		     
		     (mapcar (lambda (parm)
			       (if (consp parm)
				   (first parm)
				   (progn (setf rest t)
					  parm)))
			     parampos)))
	 (args params)
	 (lname$ (symbol-name lname))
	 (genname (intern (concatenate 'string lname$ "&"))))
    (when rest
      (setf params (append params (list 'rest)))
      (setf (car (last args)) 'rest))
    
    (case hint
      ;; Some functions return an bool-int for error checking...
      (:maybe-bool
       `(progn
	  (defcfun (,cname ,genname) ,@body)
	  (defun ,exname (,@params)
	    (declare (optimize (speed 3) (safety 0)))
	    (let ((val (,genname ,@args) ))
	      (if (= -1 val)
		  (err)
		  (plusp val))))
	  (export ',exname)))

      ;; final means create a passthrough
      (:final
       `(progn
	  (defcfun (,cname ,genname) ,@body)
	  (declaim (inline ,exname))
	  (defun ,exname (,@params)
	    (,genname ,@args))
	  (export ',exname)))
      (:bare ;; just the low-level, no export
       `(defcfun (,cname ,genname) ,@body)    )

      (T
       (case rettype
	 (:int
	  `(progn (defcfun (,cname ,genname) ,@body)
		  (defun ,exname (,@params)
		    (declare (optimize (speed 3) (safety 0)))
		    (let ((val (,genname ,@args) ))
		      (if (= -1 val)
			  (err)
			  val)))
		  (export ',exname)))
	 ((:string :pointer)
	  `(progn
	     (defcfun (,cname ,genname) ,@body)
	     (defun ,exname (,@params)
	       (declare (optimize (speed 3) (safety 0)))
	       (let ((val (,genname ,@args)))
		 (if (null-pointer-p val)
		     (err)
		     val)))
	     (export ',exname))))))))
;; Define a checked version of an & function


;; A shortcut for creating a group of exported ncurses functions -
;; - original function
;; - w- prefixed function
;; - unless mv is nil, mv- prefixed functions.
(defmacro defcurse ((cname lname &key  (mv t)  (ret :int) ) &rest parmlist)
  (let (	(mv-functions
	 (and mv
	      `((xdefcfun (,(format nil "mv~A" cname)
			    ,(intern (format nil "MV~A" lname)) :maybe-int) ,ret
			    (y  :INT) 
			    (x  :INT) 
			    ,@parmlist)
		(xdefcfun (,(format nil "mvw~A" cname)
			    ,(intern (format nil "MVW~A" lname)):maybe-int) ,ret
			    (win   (:pointer (:struct win)))
			    (y  :INT) ;;  #<VBASE :int>
			    (x  :INT) ;;  #<VBASE :int>
			    ,@parmlist)))))
    `(progn
       ;; define low-level cffi bindings
       (xdefcfun (,(format nil "~A" cname) ,(intern (format nil "~A" lname)) ) ,ret
		 ,@parmlist)
       (xdefcfun (,(format nil "w~A" cname) ,(intern (format nil "W~A" lname)) ) ,ret
		 (win   (:pointer (:struct win))) 
		 ,@parmlist)
       ,@mv-functions   )))

;;
;; Low-level: C structures
;;------------------------------------------------------------------------------
;; (/usr/include/ncurses.h:459:9 /usr/include/ncurses.h:459:9)
(defcstruct pdat ;; pdat
  (_pad_y  :SHORT) ;; _pad_y #<VBASE :short>
  (_pad_x  :SHORT) ;; _pad_x #<VBASE :short>
  (_pad_top  :SHORT) ;; _pad_top #<VBASE :short>
  (_pad_left  :SHORT) ;; _pad_left #<VBASE :short>
  (_pad_bottom  :SHORT) ;; _pad_bottom #<VBASE :short>
  (_pad_right  :SHORT) ;; _pad_right #<VBASE :short>
)
(export 'pdat)
;;------------------------------------------------------------------------------
;; (/usr/include/ncurses.h:421:8 /usr/include/ncurses.h:372:16
;;    /usr/include/ncurses.h:372:16
(defcstruct win ;; win_st
  (_cury  :SHORT) ;; _cury #<VBASE :short>
  (_curx  :SHORT) ;; _curx #<VBASE :short>
  (_maxy  :SHORT) ;; _maxy #<VBASE :short>
  (_maxx  :SHORT) ;; _maxx #<VBASE :short>
  (_begy  :SHORT) ;; _begy #<VBASE :short>
  (_begx  :SHORT) ;; _begx #<VBASE :short>
  (_flags  :SHORT) ;; _flags #<VBASE :short>
  (_attrs  :ULONG) ;; _attrs #<typedef attr_t>
  (_bkgd  :ULONG) ;; _bkgd #<typedef chtype>
  (_notimeout  (:BOOL)) ;; _notimeout #<VBASE :_Bool>
  (_clear  (:BOOL)) ;; _clear #<VBASE :_Bool>
  (_leaveok  (:BOOL)) ;; _leaveok #<VBASE :_Bool>
  (_scroll  (:BOOL)) ;; _scroll #<VBASE :_Bool>
  (_idlok  (:BOOL)) ;; _idlok #<VBASE :_Bool>
  (_idcok  (:BOOL)) ;; _idcok #<VBASE :_Bool>
  (_immed  (:BOOL)) ;; _immed #<VBASE :_Bool>
  (_sync  (:BOOL)) ;; _sync #<VBASE :_Bool>
  (_use_keypad  (:BOOL)) ;; _use_keypad #<VBASE :_Bool>
  (_delay  :INT) ;; _delay #<VBASE :int>
  (_line  :pointer ) ;; _line #<POINTER #<struct ldat>>
  (_regtop  :SHORT) ;; _regtop #<VBASE :short>
  (_regbottom  :SHORT) ;; _regbottom #<VBASE :short>
  (_parx  :INT) ;; _parx #<VBASE :int>
  (_pary  :INT) ;; _pary #<VBASE :int>
  (_parent  (:pointer (:struct win))) ;; _parent #<POINTER #<typedef WINDOW>>
  (_pad  (:struct pdat)) ;; _pad #<struct pdat>
  (_yoffset  :SHORT) ;; _yoffset #<VBASE :short>
)
(export 'win)


(defcstruct panel
  (win (:pointer (:struct win)))
  (below (:pointer (:struct panel)))
  (above(:pointer (:struct panel)))
  (user :pointer))


;; an exported constant.



;;==============================================================================
;;http://invisible-island.net/ncurses/man/curs_attr.3x.html
(xdefconst A-NORMAL     #x00000000)
(xdefconst A-STANDOUT   #x00010000)
(xdefconst A-UNDERLINE  #x00020000)
(xdefconst A-REVERSE    #x00040000)
(xdefconst A-BLINK      #x00080000)
(xdefconst A-DIM        #x00100000)
(xdefconst A-BOLD       #x00200000)
(xdefconst A-ALTCHARSET #x00400000)
(xdefconst A-INVIS      #x00800000)
(xdefconst A-PROTECT    #x01000000)
(xdefconst A-HORIZONTAL #x02000000)
(xdefconst A-LEFT       #x04000000)
(xdefconst A-LOW        #x08000000)
(xdefconst A-RIGHT      #x10000000)
(xdefconst A-TOP        #x20000000)
(xdefconst A-VERTICAL   #x40000000)
(xdefconst A-ITALIC     #x80000000)

;;==============================================================================
;;http://invisible-island.net/ncurses/man/curs_mouse.3x.html
(defcstruct mevent ;; _MEVENT
  (id  :SHORT) ;; id #<VBASE :short>
  (x  :INT) ;; x #<VBASE :int>
  (y  :INT) ;; y #<VBASE :int>
  (z  :INT) ;; z #<VBASE :int>
  (bstate  :ULONG) ;; bstate #<typedef mmask_t>
)
(export 'mevent)

(xdefconst BUTTON1-RELEASED            #x1)
(xdefconst BUTTON1-PRESSED             #x2)
(xdefconst BUTTON1-CLICKED             #x4)
(xdefconst BUTTON1-DOUBLE-CLICKED     #x10)
(xdefconst BUTTON1-TRIPLE-CLICKED     #x20)

(xdefconst BUTTON2-RELEASED           #x40)
(xdefconst BUTTON2-PRESSED            #x80)
(xdefconst BUTTON2-CLICKED           #x100)
(xdefconst BUTTON2-DOUBLE-CLICKED    #x400)
(xdefconst BUTTON2-TRIPLE-CLICKED    #x800)

(xdefconst BUTTON3-RELEASED         #x1000)
(xdefconst BUTTON3-PRESSED          #x2000)
(xdefconst BUTTON3-CLICKED          #x4000)
(xdefconst BUTTON3-DOUBLE-CLICKED  #x10000)
(xdefconst BUTTON3-TRIPLE-CLICKED  #x20000)

(xdefconst BUTTON4-RELEASED        #x40000)
(xdefconst BUTTON4-PRESSED         #x80000)
(xdefconst BUTTON4-CLICKED        #x100000)
(xdefconst BUTTON4-DOUBLE-CLICKED #x400000)
(xdefconst BUTTON4-TRIPLE-CLICKED #x800000)

(xdefconst BUTTON-CTRL           #x1000000)
(xdefconst BUTTON-SHIFT          #x2000000)
(xdefconst BUTTON-ALT            #x4000000)
(xdefconst REPORT-MOUSE-POSITION #x8000000)



(xdefconst KEY-BREAK       #o401)
(xdefconst KEY-DOWN        #o402)
(xdefconst KEY-UP          #o403)
(xdefconst KEY-LEFT        #o404)
(xdefconst KEY-RIGHT       #o405)
(xdefconst KEY-HOME        #o406)
(xdefconst KEY-BACKSPACE   #o407)
(xdefconst KEY-F0          #o410)


(xdefconst KEY-DL          #o510)
(xdefconst KEY-IL          #o511)
(xdefconst KEY-DC          #o512)
(xdefconst KEY-IC          #o513)
(xdefconst KEY-EIC         #o514)
(xdefconst KEY-CLEAR       #o515)
(xdefconst KEY-EOS         #o516)
(xdefconst KEY-EOL         #o517)
(xdefconst KEY-SF          #o520)
(xdefconst KEY-SR          #o521)
(xdefconst KEY-NPAGE       #o522)
(xdefconst KEY-PPAGE       #o523)
(xdefconst KEY-STAB        #o524)
(xdefconst KEY-CTAB        #o525)
(xdefconst KEY-CATAB       #o526)
(xdefconst KEY-ENTER       #o527)
(xdefconst KEY-SRESET      #o530)
(xdefconst KEY-RESET       #o531)
(xdefconst KEY-PRINT       #o532)
(xdefconst KEY-LL          #o533)
(xdefconst KEY-A1          #o534)
(xdefconst KEY-A3          #o535)
(xdefconst KEY-B2          #o536)
(xdefconst KEY-C1          #o537)
(xdefconst KEY-C3          #o540)
(xdefconst KEY-BTAB        #o541)
(xdefconst KEY-BEG         #o542)
(xdefconst KEY-CANCEL      #o543)
(xdefconst KEY-CLOSE       #o544)
(xdefconst KEY-COMMAND     #o545)
(xdefconst KEY-COPY        #o546)
(xdefconst KEY-CREATE      #o547)
(xdefconst KEY-END         #o550)
(xdefconst KEY-EXIT        #o551)
(xdefconst KEY-FIND        #o552)
(xdefconst KEY-HELP        #o553)
(xdefconst KEY-MARK        #o554)
(xdefconst KEY-MESSAGE     #o555)
(xdefconst KEY-MOVE        #o556)
(xdefconst KEY-NEXT        #o557)
(xdefconst KEY-OPEN        #o560)
(xdefconst KEY-OPTIONS     #o561)
(xdefconst KEY-PREVIOUS    #o562)
(xdefconst KEY-REDO        #o563)
(xdefconst KEY-REFERENCE   #o564)
(xdefconst KEY-REFRESH     #o565)
(xdefconst KEY-REPLACE     #o566)
(xdefconst KEY-RESTART     #o567)
(xdefconst KEY-RESUME      #o570)
(xdefconst KEY-SAVE        #o571)
(xdefconst KEY-SBEG        #o572)
(xdefconst KEY-SCANCEL     #o573)
(xdefconst KEY-SCOMMAND    #o574)
(xdefconst KEY-SCOPY       #o575)
(xdefconst KEY-SCREATE     #o576)
(xdefconst KEY-SDC         #o577)
(xdefconst KEY-SDL         #o600)
(xdefconst KEY-SELECT      #o601)
(xdefconst KEY-SEND        #o602)
(xdefconst KEY-SEOL        #o603)
(xdefconst KEY-SEXIT       #o604)
(xdefconst KEY-SFIND       #o605)
(xdefconst KEY-SHELP       #o606)
(xdefconst KEY-SHOME       #o607)
(xdefconst KEY-SIC         #o610)
(xdefconst KEY-SLEFT       #o611)
(xdefconst KEY-SMESSAGE    #o612)
(xdefconst KEY-SMOVE       #o613)
(xdefconst KEY-SNEXT       #o614)
(xdefconst KEY-SOPTIONS    #o615)
(xdefconst KEY-SPREVIOUS   #o616)
(xdefconst KEY-SPRINT      #o617)
(xdefconst KEY-SREDO       #o620)
(xdefconst KEY-SREPLACE    #o621)
(xdefconst KEY-SRIGHT      #o622)
(xdefconst KEY-SRSUME      #o623)
(xdefconst KEY-SSAVE       #o624)
(xdefconst KEY-SSUSPEND    #o625)
(xdefconst KEY-SUNDO       #o626)
(xdefconst KEY-SUSPEND     #o627)
(xdefconst KEY-UNDO        #o630)
(xdefconst KEY-MOUSE       #o631)
(xdefconst KEY-RESIZE      #o632)
(xdefconst KEY-EVENT       #o633)
(eval-when (:compile-toplevel :load-toplevel :execute)
  (xdefconst KEY-MAX         #o777)
)
(xdefconst MAX-COMMAND     #.(+ KEY-MAX 128))
(xdefconst KEY-IGNORE #.(+ KEY-MAX 128 1))

(xdefconst REQ-LEFT-ITEM           #.(+ KEY-MAX 1))
(xdefconst REQ-RIGHT-ITEM          #.(+ KEY-MAX 2))
(xdefconst REQ-UP-ITEM             #.(+ KEY-MAX 3))
(xdefconst REQ-DOWN-ITEM           #.(+ KEY-MAX 4))
(xdefconst REQ-SCR-ULINE           #.(+ KEY-MAX 5))
(xdefconst REQ-SCR-DLINE           #.(+ KEY-MAX 6))
(xdefconst REQ-SCR-DPAGE           #.(+ KEY-MAX 7))
(xdefconst REQ-SCR-UPAGE           #.(+ KEY-MAX 8))
(xdefconst REQ-FIRST-ITEM          #.(+ KEY-MAX 9))
(xdefconst REQ-LAST-ITEM           #.(+ KEY-MAX 10))
(xdefconst REQ-NEXT-ITEM           #.(+ KEY-MAX 11))
(xdefconst REQ-PREV-ITEM           #.(+ KEY-MAX 12))
(xdefconst REQ-TOGGLE-ITEM         #.(+ KEY-MAX 13))
(xdefconst REQ-CLEAR-PATTERN       #.(+ KEY-MAX 14))
(xdefconst REQ-BACK-PATTERN        #.(+ KEY-MAX 15))
(xdefconst REQ-NEXT-MATCH          #.(+ KEY-MAX 16))
(xdefconst REQ-PREV-MATCH          #.(+ KEY-MAX 17))

(xdefconst MIN-MENU-COMMAND        #.(+ KEY-MAX 1))
(xdefconst MAX-MENU-COMMAND        #.(+ KEY-MAX 17))





;; Predefined colors
(xdefconst COLOR-BLACK   0)
(xdefconst COLOR-RED     1)
(xdefconst COLOR-GREEN   2)
(xdefconst COLOR-YELLOW  3)
(xdefconst COLOR-BLUE    4)
(xdefconst COLOR-MAGENTA 5)
(xdefconst COLOR-CYAN    6)
(xdefconst COLOR-WHITE   7)


(xdefconst ACS-ULCORNER (+ A-ALTCHARSET #x6C)) ; upper left corner
(xdefconst ACS-LLCORNER (+ A-ALTCHARSET #x6D)) ; lower left corner
(xdefconst ACS-URCORNER (+ A-ALTCHARSET #x6B)) ; upper right corner
(xdefconst ACS-LRCORNER (+ A-ALTCHARSET #x6A)) ; lower right corner
(xdefconst ACS-LTEE     (+ A-ALTCHARSET #x74)) ; tee pointing right
(xdefconst ACS-RTEE     (+ A-ALTCHARSET #x75)) ; tee pointing left
(xdefconst ACS-BTEE     (+ A-ALTCHARSET #x76)) ; tee pointing up
(xdefconst ACS-TTEE     (+ A-ALTCHARSET #x77)) ; tee pointing down
(xdefconst ACS-HLINE    (+ A-ALTCHARSET #x71)) ; horizontal line
(xdefconst ACS-VLINE    (+ A-ALTCHARSET #x78)) ; vertical line
(xdefconst ACS-PLUS     (+ A-ALTCHARSET #x6E)) ; large plus or crossover
(xdefconst ACS-S1       (+ A-ALTCHARSET #x6F)) ; scan line 1
(xdefconst ACS-S9       (+ A-ALTCHARSET #x73)) ; scan line 9
(xdefconst ACS-DIAMOND  (+ A-ALTCHARSET #x60)) ; diamond
(xdefconst ACS-CKBOARD  (+ A-ALTCHARSET #x61)) ; checker board (stipple)
(xdefconst ACS-DEGREE   (+ A-ALTCHARSET #x66)) ; degree symbol
(xdefconst ACS-PLMINUS  (+ A-ALTCHARSET #x67)) ; plus/minus
(xdefconst ACS-BULLET   (+ A-ALTCHARSET #x7E)) ; bullet

(xdefconst ACS-LARROW   (+ A-ALTCHARSET #x2C))
(xdefconst ACS-RARROW   (+ A-ALTCHARSET #x2B))
(xdefconst ACS-DARROW   (+ A-ALTCHARSET #x2E))
(xdefconst ACS-UARROW   (+ A-ALTCHARSET #x2D))
(xdefconst ACS-BOARD    (+ A-ALTCHARSET #x68))
(xdefconst ACS-LANTERN  (+ A-ALTCHARSET #x69))
(xdefconst ACS-BLOCK    (+ A-ALTCHARSET #x30))
;; may work...
(xdefconst ACS-S3       (+ A-ALTCHARSET #x70))
(xdefconst ACS-S7       (+ A-ALTCHARSET #x72))
(xdefconst ACS-LEQUAL   (+ A-ALTCHARSET #x79))
(xdefconst ACS-GEQUAL   (+ A-ALTCHARSET #x7A))
(xdefconst ACS-PI       (+ A-ALTCHARSET #x7B))
(xdefconst ACS-NEQUAL   (+ A-ALTCHARSET #x7C))
(xdefconst ACS-STERLING (+ A-ALTCHARSET #x7D))
;;(xdefconst ACS- )


;; menu options
(xdefconst O-ONEVALUE 1)
(xdefconst O-SHOWDESC 2)
(xdefconst O-ROWMAJOR 4)
(xdefconst O-IGNORECASE 8)
(xdefconst O-SHOWMATCH 16)
(xdefconst O-NONCYCLIC 32)
(xdefconst O-MOUSEMENU 64)
;; item
(xdefconst O-SELECTABLE 1)

;; menu errors/status codes



(xdefconst E-OK 0)
(xdefconst E-SYSTEM-ERROR -1)
(xdefconst E-BAD-ARGUMENT -2)
(xdefconst E-POSTED -3)
(xdefconst E-CONNECTED -4)
(xdefconst E-BAD-STATE -5)
(xdefconst E-NO-ROOM -6)
(xdefconst E-NOT-POSTED -7)
(xdefconst E-UNKNOWN-COMMAND -8)
(xdefconst E-NO-MATCH -9)
(xdefconst E-NOT-SELECTABLE -10)
(xdefconst E-NOT-CONNECTED -11)
(xdefconst E-REQUEST-DENIED -12)
(xdefconst E-INVALID-FIELD -13)
(xdefconst E-CURRENT -14)


(defparameter ERR-NAMES #("E-OK"
			   "E-SYSTEM-ERROR"
			   "E-BAD-ARGUMENT"
			   "E-POSTED"
			   "E-CONNECTED"
			   "E-BAD-STATE"
			   "E-NO-ROOM"
			   "E-NOT-POSTED"
			   "E-UNKNOWN-COMMAND"
			   "E-NO-MATCH"
			   "E-NOT-SELECTABLE"
			   "E-NOT-CONNECTED"
			   "E-REQUEST-DENIED"
			   "E-INVALID-FIELD"
			   "E-CURRENT"))

(defun err-name (err)
  (let ((index (- err)))
    (if (> index 14)
	"INVALID"
	(aref ERR-NAMES index))))
;;==============================================================================
;; Forms
;; Field justification
(xdefconst NO-JUSTIFICATION 0)
(xdefconst JUSTIFY-LEFT 1)
(xdefconst JUSTIFY-CENTER 2)
(xdefconst JUSTIFY-RIGHT 3)

;; field options
(xdefconst O-VISIBLE		#x0001)
(xdefconst O-ACTIVE		#x0002)
(xdefconst O-PUBLIC		#x0004)
(xdefconst O-EDIT		#x0008)
(xdefconst O-WRAP		#x0010)
(xdefconst O-BLANK		#x0020)
(xdefconst O-AUTOSKIP		#x0040)
(xdefconst O-NULLOK		#x0080)
(xdefconst O-PASSOK		#x0100)
(xdefconst O-STATIC		#x0200)
(xdefconst O-DYNAMIC_JUSTIFY	#x0400)	;;/* ncurses extension	*/
(xdefconst O-NO_LEFT_STRIP	#x0800);;	/* ncurses extension	*/

;; form options */
(xdefconst  O_NL_OVERLOAD 1) 
(xdefconst  O_BS_OVERLOAD 2)

;; form driver requests
(xdefconst REQ-NEXT-PAGE           #.(+ KEY-MAX 1))
(xdefconst REQ-PREV-PAGE           #.(+ KEY-MAX 2))
(xdefconst REQ-FIRST-PAGE          #.(+ KEY-MAX 3))
(xdefconst REQ-LAST-PAGE           #.(+ KEY-MAX 4))
(xdefconst REQ-NEXT-FIELD          #.(+ KEY-MAX 5))
(xdefconst REQ-PREV-FIELD          #.(+ KEY-MAX 6))
(xdefconst REQ-FIRST-FIELD         #.(+ KEY-MAX 7))
(xdefconst REQ-LAST-FIELD          #.(+ KEY-MAX 8))
(xdefconst REQ-SNEXT-FIELD         #.(+ KEY-MAX 9))
(xdefconst REQ-SPREV-FIELD         #.(+ KEY-MAX 10))
(xdefconst REQ-SFIRST-FIELD        #.(+ KEY-MAX 11))
(xdefconst REQ-SLAST-FIELD         #.(+ KEY-MAX 12))
(xdefconst REQ-LEFT-FIELD          #.(+ KEY-MAX 13))
(xdefconst REQ-RIGHT-FIELD         #.(+ KEY-MAX 14))
(xdefconst REQ-UP-FIELD            #.(+ KEY-MAX 15))
(xdefconst REQ-DOWN-FIELD          #.(+ KEY-MAX 16))
(xdefconst REQ-NEXT-CHAR           #.(+ KEY-MAX 17))
(xdefconst REQ-PREV-CHAR           #.(+ KEY-MAX 18))
(xdefconst REQ-NEXT-LINE           #.(+ KEY-MAX 19))
(xdefconst REQ-PREV-LINE           #.(+ KEY-MAX 20))
(xdefconst REQ-NEXT-WORD           #.(+ KEY-MAX 21))
(xdefconst REQ-PREV-WORD           #.(+ KEY-MAX 22))
(xdefconst REQ-BEG-FIELD           #.(+ KEY-MAX 23))
(xdefconst REQ-END-FIELD           #.(+ KEY-MAX 24))
(xdefconst REQ-BEG-LINE            #.(+ KEY-MAX 25))
(xdefconst REQ-END-LINE            #.(+ KEY-MAX 26))
(xdefconst REQ-LEFT-CHAR           #.(+ KEY-MAX 27))
(xdefconst REQ-RIGHT-CHAR          #.(+ KEY-MAX 28))
(xdefconst REQ-UP-CHAR             #.(+ KEY-MAX 29))
(xdefconst REQ-DOWN-CHAR           #.(+ KEY-MAX 30))
(xdefconst REQ-NEW-LINE            #.(+ KEY-MAX 31))
(xdefconst REQ-INS-CHAR            #.(+ KEY-MAX 32))
(xdefconst REQ-INS-LINE            #.(+ KEY-MAX 33))
(xdefconst REQ-DEL-CHAR            #.(+ KEY-MAX 34))
(xdefconst REQ-DEL-PREV            #.(+ KEY-MAX 35))
(xdefconst REQ-DEL-LINE            #.(+ KEY-MAX 36))
(xdefconst REQ-DEL-WORD            #.(+ KEY-MAX 37))
(xdefconst REQ-CLR-EOL             #.(+ KEY-MAX 38))
(xdefconst REQ-CLR-EOF             #.(+ KEY-MAX 39))
(xdefconst REQ-CLR-FIELD           #.(+ KEY-MAX 40))
(xdefconst REQ-OVL-MODE            #.(+ KEY-MAX 41))
(xdefconst REQ-INS-MODE            #.(+ KEY-MAX 42))
(xdefconst REQ-SCR-FLINE           #.(+ KEY-MAX 43))
(xdefconst REQ-SCR-BLINE           #.(+ KEY-MAX 44))
(xdefconst REQ-SCR-FPAGE           #.(+ KEY-MAX 45))
(xdefconst REQ-SCR-BPAGE           #.(+ KEY-MAX 46))
(xdefconst REQ-SCR-FHPAGE          #.(+ KEY-MAX 47))
(xdefconst REQ-SCR-BHPAGE          #.(+ KEY-MAX 48))
(xdefconst REQ-SCR-FCHAR           #.(+ KEY-MAX 49))
(xdefconst REQ-SCR-BCHAR           #.(+ KEY-MAX 50))
(xdefconst REQ-SCR-HFLINE          #.(+ KEY-MAX 51))
(xdefconst REQ-SCR-FBLINE          #.(+ KEY-MAX 52))
(xdefconst REQ-SCR-HFHALF          #.(+ KEY-MAX 53))
(xdefconst REQ-SCR-HBHALF          #.(+ KEY-MAX 54))
(xdefconst REQ-VALIDATION          #.(+ KEY-MAX 55))
(xdefconst REQ-NEXT-CHOICE         #.(+ KEY-MAX 56))
(xdefconst REQ-PREV-CHOICE         #.(+ KEY-MAX 57))

(xdefconst MIN-FORM-COMMAND        #.(+ KEY-MAX 1))
(xdefconst MAX-FORM-COMMAND        #.(+ KEY-MAX 57))

