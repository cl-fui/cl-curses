(in-package #:nc)
;;==============================================================================
;; (ql:quickload :cl-fu)(in-package :fu)
;; sbcl --eval "(ql:quickload '(:swank) :silent t)" --eval "(swank:create-server :port 4006 :dont-close t :style :fd-handler )"
#||
(loop for a from 0 to 255 by 8 do
	 (loop for aa from a to (+ a 8) do
	        	 (format t "~A: ~A ~A~&" aa (code-char aa) (char-name (code-char aa)))))
(defun test ()
 
  (initscr) (keypad *stdscr* t) (cbreak ) (nonl)
  (when (has-colors)
    (start-color)
    (init-pair 1 COLOR-RED COLOR-BLACK)
    (init-pair 2 COLOR-GREEN COLOR-BLACK)
    (init-pair 3 COLOR-BLUE COLOR-BLACK)
    (init-pair 7 COLOR-WHITE COLOR-BLACK))
  
  ;;(attr-set )
  (printw "hello")
  (refresh)
  (getch)
  (endwin))

(defun t1 ()
  (initscr)
  (raw)
  (cbreak )
  (keypad *stdscr* 1)
  (noecho)
  ;;(&nodelay )
  (printw "Type any character and see it in bold\\n")
  (let ((ch (getch)))
    (print ch)
    (printw "You are-%d--" :int ch)
    (refresh)
    (getch)
    (endwin)
    ch)

)
(defun termstep (&optional (win *stdscr*))
  (let ((ch (wgetch win)))
    (format t "char is ~X~&" ch)
    ;; echo printable characters... TODO: keybindings
    
    (if (= ch #.(key "ESC"))
	nil
	(if (and (< ch 256)
		 (graphic-char-p (code-char ch)))
	    (progn
	      (waddch win ch)
	      (print (aref *keytable* ch))
	      (wrefresh win)
	      t)
	    t))))

(defun files (&optional (in "~/"))
  (let ((dir (truename in)))
    (loop for subdir-path in (uiop:subdirectories "~/"); (cl-fad:list-directory path)
       for y from 0 below (getmaxy *stdscr*)
       for str = (namestring(uiop:enough-pathname subdir-path dir))
       do
       ;;(format t "~%~A" (file-namestring subdir-path))
	 (format t "~%~A" str)
	 (move y 0)
	 
	 ))
  (refresh)
  )

||#


(defun in ()
  (initscr)
  (start-color)
  (cbreak )
  (noecho)
  (keypad *stdscr* 1)
  
  (refresh)
  )

(defun out ()
  (endwin)
  )


(defun mt ()
  (init-pair 1 COLOR-RED COLOR-BLACK)
  ;;
  
  )


;;-------------
;;(defun make-newwin (h w y x) (let ((n (newwin h w y x)))))

;; Collect files in this directory in native order.
;; .dotfiles? files? directories? symlinks?
(defparameter *fm-dotfiles* t)
(defparameter *fm-subdirs* t)


(defun name-dir-p (string)
  "Does this name end with a /?"
  (char= #\/ (char string (1- (length string)))))
(defun name-dotfile-p (string)
  "Does this name start with a .?"
  (char= #\. (char string 0)))

(defparameter *wd* (truename "~/"))
(defparameter *basket* nil)
;; collect files.  We shall revisit this later
(defun files-collect (&key (select  "*.*")
			(nodot (not *fm-dotfiles*)) ;; include dotfiles?
			(nosub (not *fm-subdirs*)) ;; include subdirs?
			)
  (let ((spec (concatenate 'string    (namestring *wd*) select)))
    ;; Now, collect a list of files
    (setf *basket*
	  (loop for path in (uiop:directory* spec) ;; no symlink resolution, so this
	     for str = (namestring (uiop:enough-pathname path *wd*)); here works
	     ;;(format t "~%~A " str)
	     unless (or (and nodot (name-dotfile-p str) )
			(and nosub (name-dir-p str)))
	     ;;(format t "~%~A" (file-namestring subdir-path))
	     collect str))) )




(defun fmenu-step ()
  (let ((key (getch)))
    (if (or (= 27 key) (= 10 key))
	(= 10 key)
	(progn
	  (format t "~%Key ~A"key)
	  (multiple-value-bind (result code) (menu-driver (menu-xlate key))
	    (when result
	      (format t "~%~A for key/req ~A" result code)  ))
	  nil))))

(defun fmenu-loop ()
  (loop for end = (fmenu-step)
     when end do
       (return end)))

(defun fmenu-in (&key (rows 16) (cols 1) (opts #x7B))
  (menu-new (items-new *basket*))
  (set-menu-format rows cols)
  (setf (menu-opts) opts)
  (setf (menu-pad) (char-code #\|) )
  (menu-post) (refresh)
)
(defun fmenu-out (result)
  (let ((val (when result
	       (item-name (menu-current-item)))))
    (menu-unpost) (refresh)
    (menu-free)
    val))

(defun fmenu (&key (rows 16) (cols 1) (opts #x7B))
  (menu-new (items-new *basket*))
  (set-menu-format rows cols)
  (setf (menu-opts) opts)
  (setf (menu-pad) (char-code #\|) )
  (menu-post) (refresh)
  (fmenu-out
   (fmenu-loop))
)




;;(init-pair 2 COLOR-GREEN color-BLACK )

