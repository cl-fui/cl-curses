(in-package :nc)
(eval-when (:compile-toplevel :load-toplevel :execute)
  (push :fui *features*)
  (push :fui-panel *features*)
  (push :fui-menu *features*)
  (push :fui-form *features*))


#+asdf-unicode
(cffi:define-foreign-library libcurses
  (:darwin (:or "libncurses.dylib" "libcurses.dylib"))
  (:unix (:or "libncursesw.so.6"
              "libncursesw.so.5"))
  (:windows (:or "pdcurses" "libcurses"))
  (t (:default "libcurses")))

#-sb-unicode
(cffi:define-foreign-library libcurses
  (:darwin (:or "libncurses.dylib"
                "libcurses.dylib"))
  (:unix (:or "libncursesw.so.6"
              "libncurses.so.6"
              "libncurses.so.5"
              "libcurses"))
  (:windows (:or "pdcurses"
                 "libcurses"))
  (t (:default "libcurses")))

#+fui-panel
(progn
  (cffi:define-foreign-library libpanel
    (:unix (:or "libpanel.so"))
    (t (:default "libpanel")))
  (cffi:use-foreign-library libpanel))

#+fui-menu
(progn
  (cffi:define-foreign-library libmenu
    (:unix (:or "libmenu.so"))
    (t (:default "libmenu")))
  (cffi:use-foreign-library libmenu))

#+asdf-unicode
(progn
  (cffi:define-foreign-library libform
    (:unix (:or "libformw.so"))
    (t (:default "libformw")))
  (cffi:use-foreign-library libform))








